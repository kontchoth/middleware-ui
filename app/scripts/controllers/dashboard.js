'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
  .controller('DashboardCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
