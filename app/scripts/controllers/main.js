'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
