'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
    .controller('AboutCtrl', function() {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
    });