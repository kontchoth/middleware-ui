'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:NavigationController
 * @description
 * # Navigation Controller
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
    .controller('SideNavController', ['Authentication', '$scope', '$rootScope', '$document', '$location',
        function(Authentication, $scope, $rootScope, $document, $location) {

            $scope.init = function() {
                // if (Authentication.isAuthenticated()){
                //     Authentication.getUserInfos().then(function(res) {
                //         $scope.user = res;
                //         console.log('Current user is ', $scope.user);
                //     }, function(res){
                //         console.log('error', res);
                //     });    
                // }
                $scope.displaySideNav = false;

                $scope.currentBasePage = $location.$$url.split('/')[1];
            };


            $scope.getPageTitle = function() {
                if ($scope.currentBasePage === 'contract') {
                    $scope.title = 'Contract';
                }
            };

            $scope.showSideNav = function() {
                $scope.isAuthenticateds = Authentication.isAuthenticated();
                return !Authentication.isAuthenticated();
            };

            $scope.$watch(function() {
                return $location.$$url;
            }, function(value) {
                $scope.currentBasePage = value.split('/')[1];
            });

            $rootScope.$on('$toggleSideNavigation', function(evt, data) {
                if (data) {
                    // here we hide it if it is showing otherwise ignore
                    $scope.displaySideNav = false;
                    $scope.$digest();
                } else {
                    $scope.displaySideNav = !$scope.displaySideNav;
                }
            });
        }
    ]);