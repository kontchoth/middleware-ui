'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:NavigationController
 * @description
 * # Navigation Controller
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
    .controller('NavigationController', ['Authentication', '$scope', '$rootScope', '$document', '$location',
        function(Authentication, $scope, $rootScope, $document, $location) {

            $scope.init = function() {
                $scope.showAppPopover = false;
                $scope.showNotificationPopover = false;
                $scope.showUserPopover = false;
                $scope.waitingForUserInfo = true;
                if (Authentication.isAuthenticated()) {
                    Authentication.getUserInfos().then(function(res) {
                        $scope.user = res;
                        $scope.waitingForUserInfo = false;
                    }, function(res) {
                        console.log('error', res);
                    });
                }
            };




            $scope.hideAllPopOver = function() {
                $scope.showAppPopover = false;
                $scope.showNotificationPopover = false;
                $scope.showUserPopover = false;
            };

            $scope.showMenu = function() {
                return Authentication.isAuthenticated();
            };

            $scope.showPopover = function(name) {
                // hide all popover and show corresponding one
                $scope.hideAllPopOver();
                if (name === 'user') {
                    $scope.showUserPopover = true;
                } else if (name === 'notification') {
                    $scope.showNotificationPopover = true;
                } else if (name === 'app') {
                    $scope.showAppPopover = true;
                } else {
                    // do nothing
                }
            };

            $scope.logout = function() {
                Authentication.logout();
                $location.path('/login');
            };

            $document.on('click', function(event) {
                if (angular.element(event.target).parents('.nav-ul').length === 0) {
                    // close all popover
                    $scope.hideAllPopOver();
                    $scope.$apply();
                }

                if (angular.element(event.target).parents('#sideNav').length === 0) {
                    if (angular.element(event.target).parents('.sidebar-toggler').length === 0) {
                        $rootScope.$emit('$toggleSideNavigation', true);
                    }
                }
            });

            $scope.toggleSideNavigation = function() {
                $rootScope.$emit('$toggleSideNavigation', false);
            };


            $rootScope.$on("$locationChangeStart", function(event, next, current) {
                if (Authentication.isAuthenticated()) {
                    Authentication.getUserInfos().then(function(res) {
                        $scope.user = res;
                        $scope.waitingForUserInfo = false;
                    }, function(res) {
                        console.log('error', res);
                    });
                }
            });

        }
    ]);