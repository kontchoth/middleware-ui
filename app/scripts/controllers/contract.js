'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
    .controller('ContractCtrl', ['Authentication', '$scope',
        function(Authentication, $scope) {
            $scope.init = function() {
                Authentication.getUserInfos().then(function(res) {
                    $scope.user = res;
                }, function(error) {
                    console.log('pahe title header', res);
                });
            };
        }
    ]);