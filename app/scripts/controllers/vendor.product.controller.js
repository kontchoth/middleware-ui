'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:VendorctrlCtrl
 * @description
 * # VendorctrlCtrl
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
    .controller('VendorProductCtrl', ['$location', '$mdDialog', 'Authentication', 'Vendor', 'VendorProduct', '$stateParams', '$scope',
        function($location, $mdDialog, Authentication, Vendor, VendorProduct, $stateParams, $scope) {
            $scope.new_product = false;
            $scope.vendorProducts = [];
            $scope.productNames = [];
            $scope.errors = [];
            $scope.messages = [];
            $scope.ready = false;
            $scope.notificationTime = 5000;
            $scope.units = [
                'pounds', 'kg', 'liters'
            ];

            $scope.init = function() {
                if (Authentication.isAuthenticated()) {
                    Authentication.getUserInfos().then(function(res) {
                        $scope.user = res;
                    }, function(error) {
                        console.log('pahe title header', error);
                    });

                    Vendor.queryVendor($stateParams.vendor_id).then(function(response) {
                        $scope.vendor = response.data;
                        Vendor.queryVendorProducts($stateParams.vendor_id).then(function(response) {
                            $scope.vendorProducts = response.data;
                            $scope.productNames = response.productNames;
                        }, function(error) {
                            console.log(error);
                            $scope.errors.push(CUSTOM_MSG.errorGettingProducts);
                            setTimeout(function() {
                                removeNotification(CUSTOM_MSG.errorGettingProducts, 'error');
                            }, $scope.notificationTime);
                        });
                    }, function(error) {
                        console.log(error);
                    });
                }
                $scope.message = 'Please select a vendor you would like to review.';

                if ($stateParams.vendor_id && $stateParams.product_id) {
                    $scope.initOrGetProduct($stateParams.vendor_id, $stateParams.product_id);
                }
            };

            $scope.removeNotification = function(msg, type) {
                var index = -1;
                switch (type) {
                    case 'error':
                        index = $scope.errors.indexOf(msg);
                        if (index > -1) {
                            $scope.errors.splice(index, 1);
                        }
                        break;
                    case 'message':
                        index = $scope.messages.indexOf(msg);
                        if (index > -1) {
                            $scope.messages.splice(index, 1);
                        }
                        break;
                    default:
                        break;
                }
            };

            $scope.initOrGetProduct = function(vendor_id, product_id) {
                if (product_id === 'new') {
                    $scope.new_product = true;
                    $scope.product = {
                        vendor_id: vendor_id,
                        name: '',
                        unit: 'pounds',
                        unit_qty: 0,
                        unit_price: 0
                    };
                } else {
                    VendorProduct.queryVendorProducts(vendor_id, product_id).then(function(response) {
                        $scope.product = response.data;
                        $scope.productNames = response.productNames;
                    }, function(error) {
                        console.log(error);
                    });
                }
            };

            $scope.saveOrUpdate = function() {
                if ($scope.new_product) {
                    // check to see if there is a duplicate
                    var duplicate = $scope.productNames.indexOf($scope.product.name.toLowerCase()) > -1;
                    if (duplicate) {
                        var index = $scope.errors.indexOf(CUSTOM_MSG.duplicate);
                        if (index === -1) {
                            $scope.errors.push(CUSTOM_MSG.duplicate);
                            setTimeout(function() {
                                removeNotification(CUSTOM_MSG.duplicate, 'error');
                            }, $scope.notificationTime);
                        }

                    } else {
                        VendorProduct.addVendorProduct($stateParams.vendor_id, $scope.product).then(function(response) {
                            if ($scope.debug) {
                                console.log(response);
                            }
                            $location.path('vendors/' + $stateParams.vendor_id);
                        }, function(error) {
                            console.log(error);
                        });
                    }
                } else {
                    // update the information
                    VendorProduct.updateProduct($stateParams.vendor_id, $scope.product).then(function(response) {
                        if ($scope.debug) {
                            console.log(response);
                        }

                        $scope.messages.push(CUSTOM_MSG.updateSuccess);
                        setTimeout(function() {
                            removeNotification(CUSTOM_MSG.updateSuccess, 'message');
                        }, $scope.notificationTime);
                    }, function(error) {
                        if ($scope.debug) {
                            console.log(error);
                        }
                        $scope.errors.push(CUSTOM_MSG.updateError);
                        setTimeout(function() {
                            removeNotification(CUSTOM_MSG.updateError, 'error');
                        }, $scope.notificationTime);
                    });
                }
            };

            $scope.deleteProduct = function(product) {
                VendorProduct.deleteProduct($stateParams.vendor_id, product).then(function(response) {
                    if ($scope.debug) {
                        console.log(response);
                    } else {
                        $location.path('vendors/' + $stateParams.vendor_id);
                        $scope.messages.push(CUSTOM_MSG.deleteSuccess);
                        setTimeout(function() {
                            removeNotification(CUSTOM_MSG.deleteSuccess, 'message');
                        }, $scope.notificationTime);
                    }
                }, function(errors) {
                    if ($scope.debug) {
                        console.log(errors);
                    }
                    $scope.errors.push(CUSTOM_MSG.deleteError);
                    setTimeout(function() {
                        removeNotification(CUSTOM_MSG.deleteError, 'error');
                    }, $scope.notificationTime);
                });
            };

            $scope.confirmDialog = function(ev, product) {
                $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'views/directives/generic.dialog.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: false
                    })
                    .then(function() {
                        $scope.deleteProduct(product);

                    }, function(cancel) {
                        if ($scope.debug) {
                            console.log(cancel);
                        }
                    });
            };

            function DialogController($scope, $mdDialog) {
                $scope.title = 'Confirmation Dialog';
                $scope.msg = 'Are you sure you want to delete this product?';
                $scope.hide = function() {
                    $mdDialog.hide();
                };

                $scope.cancel = function() {
                    $mdDialog.cancel();
                };

                $scope.answer = function(answer) {
                    $mdDialog.hide(answer);
                };
            }

            function removeNotification(msg, msg_type) {
                var index = (msg_type === 'error') ? $scope.errors.indexOf(msg) : $scope.messages.indexOf(msg);
                console.log('removing message at index', index);
                if (index > -1) {
                    switch (msg_type) {
                        case 'error':
                            $scope.errors.splice(index, 1);
                            $scope.$apply();
                            break;
                        case 'message':
                            $scope.messages.splice(index, 1);
                            $scope.$apply();
                            break;
                        default:
                            break;
                    }
                }
            }

            var CUSTOM_MSG = {
                duplicate: 'There is already a vendor with that name in the system.',
                updateSuccess: 'Vendor information has been updated.',
                updateError: 'There was an error updating the vendor information. Please try again later.',
                errorGettingProducts: 'There was an error getting the vendor products. Please try again later.',
                deleteError: 'There was an error deleting this product. Please try again later.',
                deleteSuccess: 'Product has been deleted properly.'
            };
        }
    ]);