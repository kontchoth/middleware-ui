'use strict';

/**
 * @ngdoc function
 * @name middlewareUiApp.controller:LogincontrollerCtrl
 * @description
 * # LogincontrollerCtrl
 * Controller of the middlewareUiApp
 */
angular.module('middlewareUiApp')
    .controller('LoginController', ['Authentication', '$scope', '$location',
        function(Authentication, $scope, $location) {
            $scope.creds = {
                email: '',
                password: ''
            };

            $scope.errors = [];

            $scope.init = function() {
                if (Authentication.isAuthenticated()) {
                    $location.path('/contract');
                }
            };

            $scope.login = function() {
                $scope.errors = [];
                Authentication.authenticate($scope.creds, function(response) {
                    if (response.status === 'success') {
                        $location.path('/contract');
                    } else {
                        $scope.errors = response.data.reasons;
                    }
                });
            };
        }
    ]);