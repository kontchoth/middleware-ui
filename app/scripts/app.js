'use strict';

/**
 * @ngdoc overview
 * @name middlewareUiApp
 * @description
 * # middlewareUiApp
 *
 * Main module of the application.
 */
angular
    .module('middlewareUiApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ui.router',
        // 'ngTouch',
        'angular-jwt',
        'LocalStorageModule',
        'angularUtils.directives.dirPagination',
        'ngMaterial',
        'ngMdIcons',
    ])
    .config(function(localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('middleware-ui')
            .setStorageType('localStorage')
            .setDefaultToCookie(false)
            .setNotify(true, true);
    })
    .config(['$qProvider', function($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }])
    .config(["$locationProvider", function($locationProvider) {
        $locationProvider.html5Mode(true);
    }])

.config(function($httpProvider, jwtOptionsProvider) {
    jwtOptionsProvider.config({
        tokenGetter: ['options', 'localStorageService', function(options, localStorageService) {
            if (options && options.url.substr(options.url.length - 5) === '.html') {
                return null;
            }

            if (options) {
                //console.log('Cannot skipped', options.url);
            }
            var token = localStorageService.get('token');
            return token;
        }],
        whiteListedDomains: ['localhost', 'middleware.hermannit.com', 'middlewareserver.hermannit.com']
    });

    $httpProvider.interceptors.push('jwtInterceptor');
})

.run(function(authManager) {
        authManager.checkAuthOnRefresh();
    })
    .run(function($rootScope, $location, Authentication) {

        // enumerate routes that don't need authentication
        var routesThatDontRequireAuth = ['/login'];

        // check if current location matches route  
        var routeClean = function(route) {
            return routesThatDontRequireAuth.find(
                function(noAuthRoute) {
                    return route.startsWith(noAuthRoute);
                }
            );
        };
        $rootScope.$on('$routeChangeStart', function(event, next, current) {
            // if route requires auth and user is not logged in
            if (!routeClean($location.url()) && !Authentication.isAuthenticated()) {
                // redirect back to login
                $location.path('/login');
            }
        });
    })
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        // .when('/', {
        //   templateUrl: 'views/main.html',
        //   controller: 'MainCtrl',
        //   controllerAs: 'main'
        // })
            .state('login', {
                url: '/login',
                views: {
                    'header': {
                        templateUrl: 'views/header.html',
                        controller: 'NavigationController',
                        controllerAs: 'navCtrl',
                    },
                    'content': {
                        templateUrl: 'views/logins/login.html',
                        controller: 'LoginController',
                        controllerAs: 'loginCtrl'
                    }

                }
            })
            .state('dashboard', {
                url: '/dashboard',
                views: {
                    'sidenav': {
                        templateUrl: 'views/side-navigation.html',
                        controller: 'SideNavController',
                        controllerAs: 'sideCtrl'
                    },
                    'content': {
                        templateUrl: 'views/dashboard.html',
                        controller: 'DashboardCtrl',
                        controllerAs: 'dashCtrl'
                    }
                }
            })
            .state('about', {
                url: '/about',
                views: {
                    'content': {
                        templateUrl: 'views/about.html',
                        controller: 'AboutCtrl',
                        controllerAs: 'about'
                    }
                }

            })
            .state('contract', {
                url: '/contract',
                views: {
                    'sidenav': {
                        templateUrl: 'views/side-navigation.html',
                        controller: 'SideNavController',
                        controllerAs: 'sideCtrl'
                    },
                    'content': {
                        templateUrl: 'views/contract.html',
                        controller: 'ContractCtrl',
                        controllerAs: 'contractCtrl',
                    },
                    'header': {
                        templateUrl: 'views/header.html',
                        controller: 'NavigationController',
                        controllerAs: 'navCtrl',
                    }
                }
            })
            .state('vendors', {
                url: '/vendors',
                views: {
                    'sidenav': {
                        templateUrl: 'views/side-navigation.html',
                        controller: 'SideNavController',
                        controllerAs: 'sideCtrl'
                    },
                    'header': {
                        templateUrl: 'views/header.html',
                        controller: 'NavigationController',
                        controllerAs: 'navCtrl',
                    },
                    'content': {
                        templateUrl: 'views/vendors/vendor.list.html',
                        controller: 'VendorCtrl',
                        controllerAs: 'vendorCtrl'
                    }
                }

            })
            .state('vendordetail', {
                url: '/vendors/:vendor_id',
                views: {
                    'sidenav': {
                        templateUrl: 'views/side-navigation.html',
                        controller: 'SideNavController',
                        controllerAs: 'sideCtrl'
                    },
                    'header': {
                        templateUrl: 'views/header.html',
                        controller: 'NavigationController',
                        controllerAs: 'navCtrl',
                    },
                    'content': {
                        templateUrl: 'views/vendors/vendor.details.html',
                        controller: 'VendorCtrl',
                        controllerAs: 'vendorCtrl'
                    }
                }
            })
            .state('vendorproducts', {
                url: '/vendors/:vendor_id/:product_id',
                views: {
                    'sidenav': {
                        templateUrl: 'views/side-navigation.html',
                        controller: 'SideNavController',
                        controllerAs: 'sideCtrl'
                    },
                    'header': {
                        templateUrl: 'views/header.html',
                        controller: 'NavigationController',
                        controllerAs: 'navCtrl',
                    },
                    'content': {
                        templateUrl: 'views/vendors/product.details.html',
                        controller: 'VendorProductCtrl',
                        controllerAs: 'vendorProductCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/login');
    });