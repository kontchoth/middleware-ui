'use strict';

/**
 * @ngdoc directive
 * @name middlewareUiApp.directive:validator
 * @description
 * # validator
 */
angular.module('middlewareUiApp')
    .directive('validator', function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {
                datalist: '='
            },
            link: function(scope, element, attrs, ngModel) {
                console.log(scope.datalist);
                var datalist = attrs.dataList;
                console.log('datalist is', datalist);
                var validatorType = attrs.validator;
                ngModel.$parsers.unshift(function(value) {
                    if (validatorType === 'unique') {
                        if (value) {
                            value = value.toLowerCase();
                        }
                        var valid = datalist.indexOf(value) === -1;
                        ngModel.$setValidity('unique', valid);
                        return valid ? value : undefined;
                    }
                });

                ngModel.$formatters.unshift(function(value) {
                    if (value) {
                        value = value.toLowerCase();
                    }
                    ngModel.$setValidity('unique', datalist.indexOf(value) === -1);

                    return value;
                });
            }
        };
    });