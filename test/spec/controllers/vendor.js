'use strict';

describe('Controller: VendorctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('middlewareUiApp'));

  var VendorctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VendorctrlCtrl = $controller('VendorctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(VendorctrlCtrl.awesomeThings.length).toBe(3);
  // });
});
