# middleware-ui

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

Run `npm install -g grunt-cli bower yo generator-karma generator-angular` to install all needed
Run `gem install compass` if you plan on using compass

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
